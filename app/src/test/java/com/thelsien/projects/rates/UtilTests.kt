package com.thelsien.projects.rates

import com.thelsien.projects.rates.util.addTo
import com.thelsien.projects.rates.util.createCurrencyRate
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.TestSubscriber
import junit.framework.Assert.assertTrue
import org.junit.Test

class UtilTests {

    @Test
    fun `createCurrencyRate() creates a correct currency rate object`() {
        // given
        val code = "EUR"
        val currencyName = "Euro"
        val rate = 100.0
        val isBase = true

        // when
        val createdCurrencyRate = createCurrencyRate(code, rate, currencyName, isBase)

        // then
        assert(
            createdCurrencyRate.isBase &&
                    createdCurrencyRate.code == code &&
                    createdCurrencyRate.rate == rate &&
                    createdCurrencyRate.currencyName == currencyName &&
                    createdCurrencyRate.countryCode == code.substring(0, 2)
        )
    }

    @Test
    fun `extension function addTo works correctly`() {
        // given
        val disposables = CompositeDisposable()
        val testSubscriber = TestSubscriber<Int>()
        Flowable.just(1).subscribe(testSubscriber)

        // when
        testSubscriber.addTo(disposables)

        // then
        assertTrue("disposables size should be 1.", disposables.size() == 1)
    }
}