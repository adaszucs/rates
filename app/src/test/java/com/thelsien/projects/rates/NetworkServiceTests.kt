package com.thelsien.projects.rates

import com.thelsien.projects.rates.data.RESTCountryResponse
import com.thelsien.projects.rates.data.RevolutRatesResponse
import com.thelsien.projects.rates.network.*
import io.reactivex.subscribers.TestSubscriber
import org.junit.Assert
import org.junit.Test

class NetworkServiceTests {

    private val defaultBase = "EUR"

    @Test
    fun `creating revolutRetrofit is successful`() {
        Assert.assertNotNull(revolutRetrofit)
    }

    @Test
    fun `creating restCountriesRetrofit is successful`() {
        Assert.assertNotNull(restCountriesRetrofit)
    }

    @Test
    fun `creating RESTCountriesService is successful`() {
        Assert.assertNotNull(restCountriesService)
    }

    @Test
    fun `call revolut api for base currency will return currency list`() {
        // given
        val service = revolutRetrofit.create(RevolutService::class.java)

        // when
        val testSubscriber = TestSubscriber.create<RevolutRatesResponse>()
        service.currencyRates(defaultBase).subscribe(testSubscriber)

        // then
        testSubscriber.assertComplete()
            .assertNoErrors()
            .assertValue { response ->
                response.base == defaultBase && response.rates.isJsonObject
            }
    }

    @Test
    fun `call rest countries api for currencies list`() {
        // given
        val service = restCountriesRetrofit.create(RESTCountriesService::class.java)

        // when
        val testSubscriber = TestSubscriber.create<List<RESTCountryResponse>>()
        service.allCountries().toFlowable().subscribe(testSubscriber)

        // then
        testSubscriber.assertComplete()
            .assertNoErrors()
            .assertValue { responseList ->
                responseList.isNotEmpty()
            }.assertValue { responseList ->
                responseList[0].currencies.isNotEmpty()
            }
    }
}
