package com.thelsien.projects.rates

import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.thelsien.projects.rates.data.RESTCountryCurrency
import com.thelsien.projects.rates.data.RESTCountryResponse
import com.thelsien.projects.rates.data.RevolutRatesResponse
import com.thelsien.projects.rates.ui.viewmodels.MainViewModel
import io.mockk.MockKAnnotations
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class ViewModelTests {

    val euroName = "Euro"
    val poundsName = "British pounds"
    val euroCode = "EUR"
    val poundsCode = "GBP"

    val mockRates = JsonObject()

    val viewModel: MainViewModel = MainViewModel()

    lateinit var mockRevolutResponse: RevolutRatesResponse

    lateinit var mockRESTCountryResponse: RESTCountryResponse

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        mockRates.add(poundsCode, JsonPrimitive(0.5))
        mockRevolutResponse = RevolutRatesResponse(
            euroCode,
            mockRates
        )

        mockRESTCountryResponse = RESTCountryResponse(
            listOf(
                RESTCountryCurrency(euroCode, euroName),
                RESTCountryCurrency(poundsCode, poundsName)
            )
        )
    }

    @After
    fun destroy() {
        unmockkAll()
    }

    @Test
    fun `after view model created, RatesAdapter is also created as member`() {
        // given

        // when

        // then
        assertNotNull("Adapter should be not empty.", viewModel.adapter)
    }

    @Test
    fun `can find a currency name for code`() {
        // given

        // when
        val currencyName = viewModel.findCurrencyName(poundsCode, listOf(mockRESTCountryResponse))

        // then
        assertEquals("Couldn't find the correct name for currency", poundsName, currencyName)
    }

    @Test
    fun `can create the base currency rate`() {
        // given

        // when
        val baseCurrencyRate = viewModel.createBaseCurrency(listOf(mockRESTCountryResponse))

        // then
        assert(
            baseCurrencyRate.isBase &&
                    baseCurrencyRate.code == euroCode &&
                    baseCurrencyRate.countryCode == euroCode.substring(0, 2) &&
                    baseCurrencyRate.currencyName == euroName &&
                    baseCurrencyRate.rate == 1.0
        )
    }

    @Test
    fun `parse responses completes successfully`() {
        // given

        // when
        val listOfCurrencies =
            viewModel.parseResponses(mockRevolutResponse, listOf(mockRESTCountryResponse))

        // then
        assertTrue("Shouldn't be empty.", listOfCurrencies.isNotEmpty())
        assertTrue("First element should be base.", listOfCurrencies[0].isBase)
        assertTrue("Second element rate should not be 0", listOfCurrencies[1].rate!! > 0)
    }
}
