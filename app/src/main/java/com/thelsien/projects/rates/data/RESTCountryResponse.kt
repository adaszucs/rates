package com.thelsien.projects.rates.data

data class RESTCountryResponse(val currencies: List<RESTCountryCurrency>)