package com.thelsien.projects.rates.data

data class RESTCountryCurrency(val code: String, val name: String)