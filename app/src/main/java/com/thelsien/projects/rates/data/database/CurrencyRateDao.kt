package com.thelsien.projects.rates.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.thelsien.projects.rates.data.CurrencyRate
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface CurrencyRateDao {

    @androidx.room.Query("SELECT * FROM currency_rate ORDER BY code ASC, isBase DESC")
    fun getCurrencyRateList(): Flowable<List<CurrencyRate>>

    @Query("SELECT * FROM currency_rate WHERE code LIKE :code")
    fun getCurrencyRateForCode(code: String): Flowable<CurrencyRate>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrencyRates(currencyRates: Iterable<CurrencyRate>): Completable

    @Query("DELETE FROM currency_rate")
    fun deleteAllCurrencyRates(): Completable
}