package com.thelsien.projects.rates.network.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import org.reactivestreams.Subscriber

class NetworkChangeBroadcastReceiver(private val subscriber: Subscriber<Boolean>) :
    BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo

        subscriber.onNext(netInfo != null && netInfo.isConnected)
    }
}
