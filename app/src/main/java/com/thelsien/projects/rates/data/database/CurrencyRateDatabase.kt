package com.thelsien.projects.rates.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.thelsien.projects.rates.data.CurrencyRate

@Database(entities = [CurrencyRate::class], version = 1)
abstract class CurrencyRateDatabase : RoomDatabase() {

    abstract fun currencyRateDao(): CurrencyRateDao

    companion object {
        @Volatile
        private var INSTANCE: CurrencyRateDatabase? = null

        fun getInstnance(context: Context): CurrencyRateDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                CurrencyRateDatabase::class.java, "currency_rate_database.db"
            ).build()
    }
}