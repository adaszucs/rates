package com.thelsien.projects.rates

import android.app.Application
import timber.log.Timber

class RatesApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }
}