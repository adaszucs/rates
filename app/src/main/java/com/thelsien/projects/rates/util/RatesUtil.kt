package com.thelsien.projects.rates.util

import com.thelsien.projects.rates.data.CurrencyRate
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.addTo(disposable: CompositeDisposable) = disposable.add(this)

fun createCurrencyRate(
    code: String,
    rate: Double,
    currencyName: String,
    isBase: Boolean
): CurrencyRate = CurrencyRate(code, rate, code.substring(0, 2), currencyName, isBase)