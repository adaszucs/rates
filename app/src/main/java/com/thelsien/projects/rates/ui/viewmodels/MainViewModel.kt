package com.thelsien.projects.rates.ui.viewmodels

import androidx.lifecycle.ViewModel
import com.thelsien.projects.rates.data.CurrencyRate
import com.thelsien.projects.rates.data.RESTCountryResponse
import com.thelsien.projects.rates.data.RevolutRatesResponse
import com.thelsien.projects.rates.data.database.CurrencyRateDao
import com.thelsien.projects.rates.network.restCountriesService
import com.thelsien.projects.rates.network.revolutService
import com.thelsien.projects.rates.ui.adapters.RatesAdapter
import com.thelsien.projects.rates.util.addTo
import com.thelsien.projects.rates.util.createCurrencyRate
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MainViewModel : ViewModel() {
    var ratesDao: CurrencyRateDao? = null
        set(value) {
            field = value
            offlineRatesStream = offlineRatesStream ?: field!!.getCurrencyRateList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

            ratesStream = ratesStream ?: Flowable.combineLatest(Flowable.defer {
                Flowable.just(baseCurrencyCodeProcessor.value!!)
            }
                .flatMap {
                    revolutService.currencyRates(baseCurrencyCodeProcessor.value!!)
                }
                .repeatWhen { flowable ->
                    flowable.concatMap { Flowable.timer(1, TimeUnit.SECONDS) }
                }
                .retry(),
                restCountriesService.allCountries().toFlowable(),
                BiFunction<RevolutRatesResponse, List<RESTCountryResponse>, List<CurrencyRate>> { revolutResponse, countriesList ->
                    parseResponses(revolutResponse, countriesList)
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }

    val disposables: CompositeDisposable = CompositeDisposable()

    val baseCurrencyCodeProcessor: BehaviorProcessor<String> =
        BehaviorProcessor.createDefault("EUR")

    val currencyRateItemsProcessor: PublishProcessor<List<CurrencyRate>> = PublishProcessor.create()

    val adapter: RatesAdapter = RatesAdapter(currencyRateItemsProcessor, baseCurrencyCodeProcessor)

    var ratesStream: Flowable<List<CurrencyRate>>? = null
    var offlineRatesStream: Flowable<List<CurrencyRate>>? = null
    var ratesSubscriber: Disposable? = null
    var offlineRatesSubscriber: Disposable? = null

    init {
        baseCurrencyCodeProcessor.subscribe {
            Timber.d("current base code: $it")
        }.addTo(disposables)
    }

    fun setupOnlineSubscriber() {
        offlineRatesSubscriber?.dispose()
        offlineRatesSubscriber = null

        ratesSubscriber = ratesSubscriber ?: ratesStream?.subscribe {
            ratesDao?.insertCurrencyRates(it)
                ?.subscribeOn(Schedulers.io())
                ?.subscribe()

            if (adapter.items.isEmpty()) {
                adapter.items.addAll(it)
                adapter.notifyDataSetChanged()
            }

            currencyRateItemsProcessor.onNext(it)
        }

        ratesSubscriber?.addTo(disposables)
    }

    fun setupOfflineSubscriber() {
        ratesSubscriber?.dispose()
        ratesSubscriber = null

        offlineRatesSubscriber =
            offlineRatesSubscriber ?: offlineRatesStream?.subscribe {
                val baseCurrency = it.find { currency -> currency.isBase }
                val newList = it.filter { currency -> !currency.isBase }.toMutableList()
                newList.add(0, baseCurrency!!)

                baseCurrencyCodeProcessor.onNext(baseCurrency.code)
                adapter.baseCurrencyProcessor.onNext(baseCurrency)

                if (adapter.items.isEmpty()) {
                    adapter.items.addAll(newList)
                    adapter.notifyDataSetChanged()
                }
            }

        offlineRatesSubscriber?.addTo(disposables)
    }

    fun parseResponses(
        revolutResponse: RevolutRatesResponse,
        countriesList: List<RESTCountryResponse>
    ): List<CurrencyRate> {
        val listOfCurrencies = ArrayList<CurrencyRate>(revolutResponse.rates.size() + 1)

        val baseCurrency = createBaseCurrency(countriesList)
        adapter.baseCurrencyProcessor.onNext(baseCurrency)
        listOfCurrencies.add(baseCurrency)

        val rates = revolutResponse.rates
        for (currencyKey in rates.keySet()) {
            listOfCurrencies.add(
                createCurrencyRate(
                    currencyKey,
                    rates.get(currencyKey).asDouble,
                    findCurrencyName(currencyKey, countriesList),
                    false
                )
            )
        }

        return listOfCurrencies
    }

    fun createBaseCurrency(countriesList: List<RESTCountryResponse>): CurrencyRate {
        return createCurrencyRate(
            baseCurrencyCodeProcessor.value!!,
            1.0,
            findCurrencyName(baseCurrencyCodeProcessor.value!!, countriesList),
            true
        )
    }

    fun findCurrencyName(code: String, countries: List<RESTCountryResponse>): String {
        return countries.find { country ->
            country.currencies.firstOrNull { it.code == code } != null
        }?.currencies?.firstOrNull {
            it.code == code
        }?.name ?: ""
    }

    override fun onCleared() {
        super.onCleared()

        adapter.disposable.clear()
        disposables.clear()
    }
}
