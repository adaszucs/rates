package com.thelsien.projects.rates.ui.adapters

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.thelsien.projects.rates.data.CurrencyRate
import com.thelsien.projects.rates.databinding.ViewCurrencyRowBinding
import com.thelsien.projects.rates.util.addTo
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.PublishProcessor


class RatesAdapter(
    private val itemsProcessor: PublishProcessor<List<CurrencyRate>>,
    private val baseCurrencyCodeProcessor: BehaviorProcessor<String>
) : RecyclerView.Adapter<RatesAdapter.ViewHolder>() {

    val baseValueProcessor: BehaviorProcessor<String> = BehaviorProcessor.createDefault("100")

    val baseCurrencyProcessor: BehaviorProcessor<CurrencyRate> = BehaviorProcessor.create()

    val disposable: CompositeDisposable = CompositeDisposable()

    val items: MutableList<CurrencyRate> = ArrayList()

    val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            //
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val stringSplit = s.toString().split(".", ",")
            val newText = if (stringSplit.size > 1) {
                "${stringSplit[0]},${stringSplit[1]}"
            } else {
                stringSplit[0]
            }
            baseValueProcessor.onNext(newText)
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ViewCurrencyRowBinding>(
            LayoutInflater.from(parent.context),
            com.thelsien.projects.rates.R.layout.view_currency_row,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    inner class ViewHolder(val binding: ViewCurrencyRowBinding) :
        RecyclerView.ViewHolder(binding.root) {

        var subsciber: Disposable? = null
        var itemSubscriber: Disposable? = null

        val inputFilter: RatesEditTextInputFilter = RatesEditTextInputFilter()

        val valueTextWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.etCurrencyValue.removeTextChangedListener(this)
                binding.etCurrencyValue.removeTextChangedListener(textWatcher)

                val stringSplit = s.toString().split(".", ",")
                val newText = if (stringSplit.size > 1) {
                    "${stringSplit[0]},${stringSplit[1]}"
                } else {
                    stringSplit[0]
                }

                binding.etCurrencyValue.setText(newText)
                binding.etCurrencyValue.setSelection(binding.etCurrencyValue.text.length)
                binding.etCurrencyValue.addTextChangedListener(this)
                binding.etCurrencyValue.addTextChangedListener(textWatcher)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //
            }
        }

        init {
            itemView.setOnClickListener {
                if (adapterPosition == 0) {
                    return@setOnClickListener
                }
                val clickedCurrency = items.removeAt(adapterPosition)
                items.add(0, clickedCurrency)
                clickedCurrency.isBase = true
                items[1].isBase = false

                baseCurrencyCodeProcessor.onNext(clickedCurrency.code)
                baseCurrencyProcessor.onNext(clickedCurrency)
                baseValueProcessor.onNext(binding.etCurrencyValue.text.toString())

                notifyItemChanged(0)
                notifyItemChanged(adapterPosition)
                notifyItemMoved(adapterPosition, 0)
            }

            binding.etCurrencyValue.filters = arrayOf(inputFilter)
            binding.etCurrencyValue.onFocusChangeListener =
                View.OnFocusChangeListener { _, hasFocus ->
                    if (hasFocus) {
                        itemView.callOnClick()
                    }
                }
        }

        fun bind(currencyRate: CurrencyRate) {
            binding.currentCurrency = currencyRate
            binding.executePendingBindings()

            itemSubscriber?.dispose()
            subsciber?.dispose()
            binding.etCurrencyValue.removeTextChangedListener(textWatcher)
            binding.etCurrencyValue.removeTextChangedListener(valueTextWatcher)

            if (currencyRate.isBase) {
                inputFilter.isBase = true
                binding.etCurrencyValue.addTextChangedListener(textWatcher)
                binding.etCurrencyValue.addTextChangedListener(valueTextWatcher)
                binding.etCurrencyValue.setText(baseValueProcessor.value!!)
            } else {
                inputFilter.isBase = false
                binding.etCurrencyValue.setText(
                    createRateString(
                        currencyRate,
                        baseValueProcessor.value!!
                    )
                )
                subsciber = baseValueProcessor.subscribe {
                    if (adapterPosition != -1) {
                        binding.etCurrencyValue.setText(
                            createRateString(
                                items[adapterPosition],
                                it
                            )
                        )
                    }
                }

                itemSubscriber = itemsProcessor.subscribe { currencyList ->
                    if (adapterPosition != -1) {
                        currencyList.forEach { newCurrency ->
                            items.firstOrNull { it.code == newCurrency.code }?.rate =
                                newCurrency.rate
                        }

                        binding.etCurrencyValue.setText(
                            createRateString(
                                items[adapterPosition],
                                baseValueProcessor.value!!
                            )
                        )
                    }
                }

                subsciber?.addTo(disposable)
                itemSubscriber?.addTo(disposable)
            }
        }

        fun createRateString(currencyRate: CurrencyRate, baseValue: String): String {
            return if (baseValue == "") {
                ""
            } else {
                ((currencyRate.rate!! / baseCurrencyProcessor.value!!.rate!!) * baseValue.replace(
                    ",",
                    "."
                ).toDouble()).toString()
            }
        }
    }
}