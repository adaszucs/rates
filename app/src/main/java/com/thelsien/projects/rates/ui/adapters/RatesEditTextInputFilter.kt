package com.thelsien.projects.rates.ui.adapters

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Pattern

class RatesEditTextInputFilter : InputFilter {

    var isBase = false

    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val string = source.toString()
        if (string.trim() == "" || string.last() == '.' || string.last() == ',') {
            return null
        }

        val pattern = Pattern.compile("^[-+]?[0-9]+([,.][0-9]*)?$")
        return if (pattern.matcher(string).matches()) {
            val splits = string.split(",", ".")
            if (splits.size == 1) {
                splits[0]
            } else {
                val secondSplitFormatted = if (splits[1].length > 2) {
                    removeTrailingZeroes(splits[1].substring(0, 2))
                } else {
                    removeTrailingZeroes(splits[1])
                }

                if (secondSplitFormatted.isNotEmpty()) {
                    "${splits[0]},$secondSplitFormatted"
                } else {
                    splits[0]
                }
            }
        } else {
            ""
        }
    }

    fun removeTrailingZeroes(s: String): String {
        val sb = StringBuilder(s)
        while (!isBase && sb.isNotEmpty() && sb[sb.length - 1] == '0') {
            sb.setLength(sb.length - 1)
        }
        return sb.toString()
    }
}