package com.thelsien.projects.rates.network

import com.thelsien.projects.rates.data.RESTCountryResponse
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RESTCountriesService {

    @GET("all")
    fun allCountries(): Single<List<RESTCountryResponse>>
}

val restCountriesRetrofit: Retrofit = Retrofit.Builder()
    .baseUrl("https://restcountries.eu/rest/v2/")
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .addConverterFactory(GsonConverterFactory.create())
    .build()

val restCountriesService: RESTCountriesService =
    restCountriesRetrofit.create(RESTCountriesService::class.java)