package com.thelsien.projects.rates.data

import com.google.gson.JsonObject

data class RevolutRatesResponse(val base: String, val rates: JsonObject)