package com.thelsien.projects.rates.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import java.util.*

@BindingAdapter("flag")
fun setFlag(view: ImageView, value: String) {
    val tag = view.tag

    if (tag == value) {
        return
    }

    view.tag = value
    val resource = view.context.resources
    val flagDrawableRes =
        resource.getIdentifier(
            "c_${value.toLowerCase(Locale.ROOT)}",
            "drawable",
            view.context.packageName
        )
    if (flagDrawableRes > 0) {
        view.setImageResource(flagDrawableRes)
    }
}