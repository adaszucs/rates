package com.thelsien.projects.rates.network

import com.thelsien.projects.rates.data.RevolutRatesResponse
import io.reactivex.Flowable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutService {

    @GET("latest")
    fun currencyRates(@Query("base") currency: String): Flowable<RevolutRatesResponse>
}

val revolutRetrofit: Retrofit = Retrofit.Builder()
    .baseUrl("https://revolut.duckdns.org/")
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .addConverterFactory(GsonConverterFactory.create())
    .build()

val revolutService: RevolutService = revolutRetrofit.create(RevolutService::class.java)
