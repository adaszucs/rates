package com.thelsien.projects.rates.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency_rate")
data class CurrencyRate(
    @PrimaryKey val code: String,
    var rate: Double?,
    var countryCode: String = "",
    var currencyName: String = "",
    var isBase: Boolean = false
)