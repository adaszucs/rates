package com.thelsien.projects.rates.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.thelsien.projects.rates.R
import com.thelsien.projects.rates.data.database.CurrencyRateDatabase
import com.thelsien.projects.rates.ui.viewmodels.MainViewModel
import com.thelsien.projects.rates.util.addTo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    private lateinit var listView: RecyclerView

    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.ratesDao = CurrencyRateDatabase.getInstnance(this).currencyRateDao()

        ReactiveNetwork
            .observeNetworkConnectivity(this)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { connectivity ->
                if (connectivity.available()) {
                    viewModel.setupOnlineSubscriber()
                } else {
                    viewModel.setupOfflineSubscriber()
                }

            }.addTo(disposables)

        setupListView()
    }

    private fun setupListView() {
        listView = findViewById(R.id.rv_list)
        listView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        listView.adapter = viewModel.adapter

        viewModel.baseCurrencyCodeProcessor.subscribe {
            listView.scrollToPosition(0)
        }.addTo(viewModel.disposables)
    }

    override fun onStop() {
        disposables.clear()

        super.onStop()
    }
}
