package com.thelsien.projects.rates.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.widget.EditText
import androidx.core.content.ContextCompat
import com.thelsien.projects.rates.R

class UnderlinedEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : EditText(context, attrs, defStyleAttr, defStyleRes) {

    private val underlinePaint = Paint()

    private val rect = Rect()

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        underlinePaint.apply {
            style = Paint.Style.STROKE
            isAntiAlias = true
            color = ContextCompat.getColor(context, R.color.underline_color)
            strokeWidth = 2f
        }

        getLineBounds(0, rect)
        val textWidth = paint.measureText(text.toString())
        val hintWidth = paint.measureText(hint.toString())

        val currentWidth = if (textWidth > 0) {
            textWidth
        } else {
            hintWidth * 2
        }

        canvas?.drawLine(
            rect.right - currentWidth,
            baseline.toFloat() + 10f,
            rect.right.toFloat(),
            baseline.toFloat() + 10f,
            underlinePaint
        )
    }
}